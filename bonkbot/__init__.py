import asyncio
import os
import discord
from discord.ext import commands
from dotenv import load_dotenv
from time import time

SOUND_RESOURCE = os.path.dirname(__file__) + '/resources/bonk.opus';

def main():
    load_dotenv()
    TOKEN = os.getenv('DISCORD_TOKEN')

    bot = commands.Bot(command_prefix='!')
    bot.jail_channel = 'Shadow Realm'
    bot.last_bonk_time = 0


    async def send_cmd_help(ctx):
        cmd = ctx.command
        em = discord.Embed(title=f'Usage: {ctx.prefix + cmd.signature}')
        em.color = discord.Color.green()
        em.description = cmd.help
        return em


    @bot.command(name='bonk', help='Sends people to horny jail.')
    @commands.has_role('bonk stick')
    async def bonk(ctx, member: discord.Member):
        channel = discord.utils.get(ctx.guild.voice_channels, name=bot.jail_channel)
        if not channel:
            await ctx.send(f'No voice channel {bot.jail_channel} exists. Use !set_jail to specify a valid channel')
            return

        if not member.voice.channel:
            await ctx.send(f'{member.name} is not connected to voice.')
            return

        # Just going to assume only one voice channel in the following block.
        if len(bot.voice_clients) == 0:
            voice = await channel.connect()
        elif bot.voice_clients[0].channel.id != channel.id:
            await bot.voice_clients[0].disconnect(force=True)
            voice = await channel.connect()
        else:
            voice = bot.voice_clients[0]

        await member.move_to(channel)
        voice.play(discord.FFmpegOpusAudio(SOUND_RESOURCE, codec='copy'))

        # Disconnect after some time. Some naive inactivity detection here.
        bonk_time = int(time())
        bot.last_bonk_time = bonk_time
        await asyncio.sleep(30)
        if bot.last_bonk_time == bonk_time and len(bot.voice_clients) != 0:
            await bot.voice_clients[0].disconnect(force=True)


    @bot.command(name='set_jail', help='Sets the channel for horny jail.')
    @commands.has_role('bonk stick')
    async def set_jail(ctx, channel: discord.VoiceChannel):
        bot.jail_channel = channel.name
        await ctx.send(f'Horny jail is now {channel.name}')


    @bot.event
    async def on_command_error(ctx, error):
        if isinstance(error, commands.errors.CheckFailure):
            await ctx.send('You do not have a bonk stick.')
        elif isinstance(error, commands.errors.MemberNotFound):
            await ctx.send(f'Member {error.argument} not found.')
        elif isinstance(error, commands.errors.ChannelNotFound):
            await ctx.send(f'Channel {error.argument} not found.')
        elif isinstance(error, commands.errors.MissingRequiredArgument):
            _help = await send_cmd_help(ctx)
            await ctx.send(embed=_help)
        elif isinstance(error, commands.errors.MissingRequiredArgument):
            await ctx.send(f'{member.name} is not in a voice channel')

        # To make errors visible.
        raise error

    @bot.event
    async def on_ready():
        print(f'{bot.user.name} has connected to Discord!')


    bot.run(TOKEN)
