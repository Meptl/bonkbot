# Bonk Bot
There is a variant of this bot which uses Discord Interactions and a serverless
architecture. Check out
[bonkbot-serverless](https://gitlab.com/Meptllc/bonkbot-serverless).

A Discord bot that sends people to horny jail.

# Usage
```
DISCORD_TOKEN=your_bot_token ./venv/bin/bonkbot
```

# Notes
Sound resources are pre-encoded as OPUS files. This has two benefits:
* Remove runtime dependency on libopus
* Performance benefits... but not enough to run on a raspberry pi zero :(
