{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  name = "bonkbot";
  venvDir = "./venv";
  buildInputs = with pkgs; [
    python37Packages.venvShellHook
  ];

  nativeBuildInputs = with pkgs; [
    # vim integration packages.
    # python37Packages.jedi # This nixpkg is too outdated.
    python38Packages.flake8

    python38Packages.python-dotenv
    ffmpeg
    libopus
  ];

  preShellHook = ''
    unset SOURCE_DATE_EPOCH
  '';
  postShellHook = ''
    # libstdc++.so.6
    export LD_LIBRARY_PATH=${pkgs.lib.makeLibraryPath [pkgs.stdenv.cc.cc]}

    # pip install -r requirements.txt
    pip install -e .
  '';
}
